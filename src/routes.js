import Register from './pages/Register.vue'
import Login from './pages/Login.vue'
import Home from './pages/Home.vue'
import ResetPassword from './pages/ResetPassword.vue'
import Profile from "./pages/Profile.vue";
import ProfileCompany from "./pages/ProfileCompany.vue";
import ListJobs from "./pages/Jobs.vue"
import Post from "./pages/Post.vue";
import UpdatePost from "./pages/UpdatePost.vue";
import CreatePost from "./pages/CreatePost.vue";
import GetProfile from './pages/GetProfile';
import ListCompany from './pages/Company'
import CompanyDetail from './pages/CompanyDetail'
import Vue from 'vue'

import Router from "vue-router";
import Vuetify from "vuetify";

Vue.use(Router);
Vue.use(Vuetify);

export default new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        name: 'home',
        component: Home
      },
      {
        path: '/login',
        component: Login
      },
      {
        path: '/register',
        component: Register
      },
      {
        path:'/reset-password',
        component: ResetPassword
      },
      {
        path: "/profile",
        component: Profile
      },
      {
        path: "/profile-company",
        component: ProfileCompany
      },
      {
        path:"/get-profile/:id",
        component: GetProfile
      },
      {
        path:"/jobs",
        component: ListJobs
      },
      {
        path:"/post/:id",
        component: Post
      },
      {
        path:"/update-post",
        component: UpdatePost
      },
      {
        path:"/create-post",
        component: CreatePost
      },
      {
        path:"/list-companies",
        component: ListCompany
      },
      {
        path:"/companies-detail/:id",
        component: CompanyDetail
      }
    ]
  })
