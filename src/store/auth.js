/* eslint-disable no-console */
import axios from 'axios';
import { isObjEmpty } from '../../utils';

export const strict = false;
const API_URL = process.env.VUE_APP_API_URL_PRODUCT

const initialState = () => ({
    user: JSON.parse(window.localStorage.getItem("userInfo")),
    image: "",
});
export const stateAuth = initialState();

export const getters = {
    getHello: (state) => {
        return state.hello
    },
};

export const actionsAuth = {
    resetPassword({ commit }, value) {
        // eslint-disable-next-line no-console
        console.log("value", value)
        return new Promise((resolve, reject) => {
            axios.post(API_URL + "/api/reset-password/confirm/", value)
                .then(response => {
                    // eslint-disable-next-line no-console
                    console.log(response)
                    resolve(response)
                    commit("")
                }, error => {
                    // eslint-disable-next-line no-console
                    console.log(error)
                    reject(error)
                })
        })
    },
    sendResetPasswordEmail({ commit }, email) {
        return new Promise((resolve, reject) => {
            axios.post(API_URL + "/api/reset-password/", { email: email })
                .then(response => {
                    // eslint-disable-next-line no-console
                    console.log(response)
                    resolve(response)
                    commit("")
                }, error => {
                    // eslint-disable-next-line no-console
                    console.log(error)
                    reject(error)
                })
        })
    },
    changeAvatarUser({ commit }, image, id){
        console.log("Can't test this one", id);
        console.log(image);
        commit("SET_IMAGE_LINK", image)
    },
    changeResumeUser({ commit }, file){
        console.log(file);
        commit("SET_RESUME", file)
    },
    changeImageCompany({ commit }, image){
        console.log(image);
        commit("SET_IMAGE_COMPANY_LINK", image)
    },
    changePassword({ commit }, value){
        // eslint-disable-next-line no-console
        console.log(value)
        return new Promise((resolve, reject) => {
            axios.put(API_URL + "/api/accounts/change-password", value)
                .then(response => {
                    console.log(response)
                    resolve(response);
                }, error => {
                    console.log(error)
                    reject(error);
                    commit("")
                })
        })
    },
    login({ commit }, userAuth){
        return new Promise((resolve, reject) => {
            axios.post(API_URL + '/api/accounts/login', userAuth)
            .then(response => {
                if(response.status == 200){
                    commit("STORE_USER_INFO", response.data)
                    window.localStorage.setItem('userInfo', JSON.stringify(response.data));
                    axios.defaults.headers.common['Authorization'] = 'Token ' + response.data.token;
                }
                resolve(response);
            }, error => {
                reject(error);
                commit("")
            })
        })
    },
    logout({ commit }){
        axios.defaults.headers.common['Authorization'] = '';
        window.localStorage.removeItem('userInfo');
        commit("REMOVE_USER_INFO")
    },
    register({ commit }, user){
        console.log(user)
        let formData = new FormData();
        Object.entries(user).forEach(([k, v]) => {
            if (k == 'company_logo') {
            formData.append(k, v, v.name)
            } else {
            formData.append(k, v)
            }
        })
        return new Promise((resolve, reject) => {
            axios.post(API_URL + '/api/accounts/users/', formData)
                .then(response => {
                    resolve(response);
                }, error => {
                    reject(error);
                    commit("")
                })
        })
    },
    saveUserInfo({ commit }, userInfo){
        console.log('saveUserInfo', userInfo)
        // const { avatarFile, resumeFile, fullname } = userInfo
        let formData = new FormData();
        Object.entries(userInfo).forEach(([k, v]) => {
            if (['token', 'id', 'name_company', 'avatar', 'resume'].includes(k)) {
                return
            }
            if (k == 'avatarFile' || k == 'resumeFile') {
                if (isObjEmpty(v)) {
                    let name = ''
                    if (k == 'avatarFile') {
                        name = 'avatar'
                    }
                    if (k == 'resumeFile') {
                        name = 'resume'
                    }
                    formData.append(name, v, v.name)
                }
            } else {
                formData.append(k, v)
            }
        })
        return new Promise((resolve, reject) => {
            axios.post(API_URL + `/api/accounts/${userInfo.id}/change-avatar`, formData, {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            })
                .then(res => {
                    const avatar = `http://localhost:8000${res.data.avatar}`
                    const resume = `http://localhost:8000${res.data.resume}`
                    userInfo.avatar = avatar
                    userInfo.resume = resume
                    commit("SET_USER_RESPONSE", userInfo)
                    resolve(res)
                }, error => {
                    console.log(error)
                    reject(error)
                })
        });
    },
    saveCompanyInfo({ commit }, companyInfo){
        console.log('saveCompanyInfo', companyInfo)
        let formData = new FormData();
        Object.entries(companyInfo).forEach(([k, v]) => {
            if (k == 'image') {
                return
            }

            if (k == 'imageFile') {
                if (isObjEmpty(v)) {
                    formData.append('image', v, v.name)
                }
            } else {
                formData.append(k, v)
            }
        })
        return new Promise((resolve, reject) => {
            axios.put(API_URL + `/api/company/${companyInfo.id}/`, formData, {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            })
                .then(res => {
                    const image = res.data.image
                    companyInfo.image = image
                    commit("SET_COMPANY_RESPONSE", companyInfo)
                    resolve(res)
                }, error => {
                    console.log(error)
                    reject(error)
                })
        });
    },

    getCandidateUserById({ commit }, id){
        return new Promise((resolve, reject) => {
            axios.get(API_URL + `/api/accounts/users/${id}/`)
                .then(response => {
                    resolve(response);
                }, error => {
                    reject(error);
                    commit("")
                })
        })
    },
}

export const mutationsAuth = {
    STORE_USER_INFO(state, user) {
        state.user = user
    },
    REMOVE_USER_INFO(state) {
        state.user = {}
    },
    SET_IMAGE_LINK(state, image) {
        state.user.avatar = URL.createObjectURL(image)
        state.user.avatarFile = image
    },
    SET_RESUME(state, file) {
        state.user.resumeFile = file
    },
    SET_IMAGE_COMPANY_LINK(state, image) {
        state.user.company.image = URL.createObjectURL(image)
        state.user.company.imageFile = image
    },
    SET_USER_RESPONSE(state, data) {
        console.log(data)
        state.user = data
        window.localStorage.setItem('userInfo', JSON.stringify(data))
    },
    SET_COMPANY_RESPONSE(state, data) {
        console.log(data)
        state.user.company = data
        window.localStorage.setItem('userInfo', JSON.stringify(state.user))
    }
};