/* eslint-disable no-console */
import axios from 'axios';
/* index.js */
const API_URL = process.env.VUE_APP_API_URL_PRODUCT

export const strict = false;

// Global State object
const initialState = () => ({
    listCompanies: [],
});
export const stateCompany = initialState();

// Global Getter functions
export const getters = {
};
  
// Global Actions
export const actionsCompany = {
    getListCompanies({ commit }, page_number) {
        return new Promise((resolve, reject) => {
            axios.get(API_URL + `/api/company/?page=${page_number}`)
                .then(response => {
                    commit("GET_LIST_COMPANIES", response.data);
                    resolve(response)
                }, error => {
                    reject(error);
                    commit("")
                })
        })
    },
};

// Global Mutations
export const mutationsCompany = {
  GET_LIST_COMPANIES(state, value){
    state.listCompanies = value;
  },
};
