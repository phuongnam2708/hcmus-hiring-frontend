import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { stateHome } from './home'
import { actionsHome } from './home'
import { mutationsHome } from './home'

import { stateAuth } from './auth'
import { actionsAuth } from './auth'
import { mutationsAuth } from './auth'

import { stateJob } from './job'
import { actionsJob } from './job'
import { mutationsJob } from './job'

import { stateCompany } from './company'
import { actionsCompany } from './company'
import { mutationsCompany } from './company'

export default new Vuex.Store({
    state: {
        ...stateHome,
        ...stateAuth,
        ...stateJob,
        ...stateCompany
    },
    actions: {
        ...actionsHome,
        ...actionsAuth,
        ...actionsJob,
        ...actionsCompany
    },
    mutations: {
        ...mutationsHome,
        ...mutationsAuth,
        ...mutationsJob,
        ...mutationsCompany
    }
})