/* eslint-disable no-console */
import axios from 'axios';
/* index.js */
const API_URL = process.env.VUE_APP_API_URL_PRODUCT

export const strict = false;

const user = JSON.parse(window.localStorage.getItem("userInfo"))
const { token } = (user || '')
if (token) {
    axios.defaults.headers.common['Authorization'] = 'Token ' + token
}

// Global State object
const initialState = () => ({
    listJobs: [],
    list_applied: [],
    post: {
        title: '',
        content: '',
        description: '',
        min_salary: 0,
        max_salary: 0,
        skills: '',
        name_company: '',
        name_author: '',
        is_ot: false,
        time: '',
    }
});
export const stateJob = initialState();

// Global Getter functions
export const getters = {
};
  
// Global Actions
export const actionsJob = {
    getListJobs({ commit }, page_number) {
        return new Promise((resolve, reject) => {
            axios.get(API_URL + `/api/posts/?page=${page_number}`)
                .then(response => {
                    commit("GET_LIST_JOBS", response.data);
                    resolve(response)
                }, error => {
                    reject(error);
                    commit("")
                })
        })
    },
    getListApplied({ commit }, user_id) {
        return new Promise((resolve, reject) => {
            axios.get(API_URL + `/api/accounts/users/${user_id}/list_applied/`)
                .then(response => {
                    commit("GET_LIST_APPLIED", response.data);
                    resolve(response)
                }, error => {
                    reject(error);
                    commit("")
                })
        })
    },
    createPost({ commit }, post) {
        const { title, description, content, min_salary, max_salary, skills, created_at, 
            name_company, 
            name_author, is_ot, time } = post
        let formData = new FormData();
        formData.append('title', title);
        formData.append('description', description);
        formData.append('content', content);
        formData.append('min_salary', min_salary);
        formData.append('max_salary', max_salary);
        formData.append('skills', skills);
        formData.append('time', time);
        formData.append('created_at', created_at);
        formData.append('name_company', name_company);
        formData.append('name_author', name_author);
        formData.append('is_ot', is_ot);
        return new Promise((resolve, reject) => {
            axios.post(API_URL + `/api/posts/`, formData)
                .then(response => {
                    resolve(response);
                    commit("CLEAR_POST");
                }, error => {
                     // eslint-disable-next-line no-console
                    console.log(error)
                    reject(error);
                    commit("")
                })
        })
    },
    applyPost({ commit }, { type, post_id }) {
        console.log('xxx', type, post_id)
        return new Promise((resolve, reject) => {
            axios.post(API_URL + `/api/posts/${post_id}/apply/`)
                .then(response => {
                    resolve(response);
                    if (type === 'list_post') {
                        commit("UPDATE_LIST_JOBS_IS_APPLY", post_id);
                    }
                }, error => {
                    reject(error);
                    commit("")
                })
        })
    },
    getPost({ commit }, post_id) {
        return new Promise((resolve, reject) => {
            axios.get(API_URL + `/api/posts/${post_id}/`)
                .then(response => {
                    resolve(response)
                }, error => {
                    reject(error);
                    commit("")
                })
        })
    },
};

// Global Mutations
export const mutationsJob = {
  GET_LIST_JOBS(state, value){
    state.listJobs = value;
  },
  GET_LIST_APPLIED(state, data){
    data = data.list_applied.map(item => item.post.id)
    state.list_applied = data;
  },
  CLEAR_POST(state){
    state.post = {
        title: '',
        content: '',
        description: '',
        min_salary: 0,
        max_salary: 0,
        skills: '',
        name_company: '',
        name_author: '',
        is_ot: false,
        time: '',
    }
  },
  UPDATE_LIST_JOBS_IS_APPLY(state, post_id){
    state.listJobs.results.forEach(job => {
        if (job.id == post_id) {
            job.is_apply = true
        }
    })
  },
};
