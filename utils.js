function isObjEmpty(obj) {
  for(var prop in obj) {
    if(obj.hasOwnProperty(prop)) {
      return false;
    }
  }

  return JSON.stringify(obj) === JSON.stringify({});
}

function isItNumber(str) {
  // eslint-disable-next-line no-useless-escape
  return /^\-?[0-9]+(e[0-9]+)?(\.[0-9]+)?$/.test(str);
}

module.exports = {
  isObjEmpty,
  isItNumber
}